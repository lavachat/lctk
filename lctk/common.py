import asyncio
import logging
import tkinter as tk

log = logging.getLogger(__name__)

class Frame(tk.Frame):
    def __init__(self, parent, app):
        super().__init__(parent)
        self.app = app

    async def _wrap_coro(self, coro):
        try:
            await coro
        except Exception:
            log.exception('error on wrapped coro')

    def _call(self, func, *args, **kwargs):
        def wrapped():
            asyncio.ensure_future(self._wrap_coro(
                func(*args, **kwargs)
            ))

        return wrapped
