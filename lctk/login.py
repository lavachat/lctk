import logging
import tkinter as tk

from lctk.common import Frame

log = logging.getLogger(__name__)


class LoginFrame(Frame):
    def __init__(self, parent, app):
        super().__init__(parent, app)

        instance_label = tk.Label(self, text='Lavachat Instance')
        user_label = tk.Label(self, text='Username')
        pwd_label = tk.Label(self, text='Password')

        instance_entry = tk.Entry(self)
        user_entry = tk.Entry(self)
        pwd_entry = tk.Entry(self, show='*')

        instance_label.grid()
        instance_entry.grid()

        user_label.grid()
        user_entry.grid()

        pwd_label.grid()
        pwd_entry.grid()

        async def _call_query_token():
            await self.query_token(
                instance_entry.get(), user_entry.get(),
                pwd_entry.get()
            )

        login_btn = tk.Button(
            self, text='Login',
            command=self._call(_call_query_token)
        )
        login_btn.grid()

    async def query_token(self, instance: str, 
                          username: str, password: str):
        log.info('querying token for instance %r', instance)

        wellknown = f'https://{instance}/.well-known/lavachat'
        async with self.app.session.get(wellknown) as resp:
            wl_json = await resp.json()

        log.debug('got wellknown: %r', wl_json)

        assert wl_json['version'] == 0
        login_url = f'{wl_json["api_url"]}/auth/login/plain'

        payload = {
            'username': username,
            'password': password
        }

        async with self.app.session.post(login_url, json=payload) as resp:
            tok_json = await resp.json()

        assert 'error' not in tok_json
        token = tok_json['token']
        self.app.cfg['token'] = token
        self.app.cfg_save()
