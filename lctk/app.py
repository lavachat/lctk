import logging
import json
import asyncio
import tkinter as tk
from pathlib import Path

import aiohttp

from lctk.login import LoginFrame

log = logging.getLogger(__name__)

CFG_PATH = Path.home() / '.config' / 'lctk.json'
DEFAULT_CFG = {}


class Application(tk.Tk):
    """Main lctk application class."""
    FRAME_CLASSES = {
        'login': LoginFrame,
    }

    def __init__(self, loop):
        super().__init__()

        # common attrs
        self.loop = loop
        self.session = aiohttp.ClientSession()
        self.cfg = None

        self.title('lctk')
        self.geometry('350x200')

        container = tk.Frame(self)
        container.pack(side='top', fill='both', expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        # initialize common frames
        self._frames = {}

        for frame_id, frame_cls in self.FRAME_CLASSES.items():
            frame = frame_cls(container, self)

            self._frames[frame_id] = frame

            # grid it so its visible in the container
            frame.grid()

    def show_frame(self, frame_id: str):
        """Show a frame."""
        try:
            frame = self._frames[frame_id]
            log.debug('showing frame: %s', frame_id)

            frame.tkraise()
        except KeyError:
            log.warning('unknown frame: %s', frame_id)

    def _init_frame(self) -> str:
        if 'token' not in self.cfg:
            return 'login'

        return 'main'

    async def _run_tk(self, interval=.05):
        try:
            initial_frame = self._init_frame()
            self.show_frame(initial_frame)

            # TODO: if frame is 'main', start other functions

            while True:
                self.update()
                await asyncio.sleep(interval)
        except tk.TclError as err:
            if 'application has been destroyed' in err.args[0]:
                return

            log.exception('error while running')
            raise err

    def _read_cfg(self):
        try:
            cfg_text = CFG_PATH.read_text()
            cfg_from_file = json.loads(cfg_text)
        except FileNotFoundError:
            CFG_PATH.write_text(json.dumps(DEFAULT_CFG))
            cfg_from_file = DEFAULT_CFG

        self.cfg = DEFAULT_CFG
        self.cfg.update(cfg_from_file)

        log.debug('config: %r', self.cfg)

    def cfg_save(self):
        log.debug('saved config')
        CFG_PATH.write_text(json.dumps(self.cfg))

    def run(self):
        """Start the app."""
        self._read_cfg()
        self.loop.run_until_complete(self._run_tk())
