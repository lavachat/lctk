import asyncio
import logging
import tkinter

from lctk.app import Application

log = logging.getLogger(__name__)


def main():
    """main entrypoint."""
    logging.basicConfig(level=logging.DEBUG)
    loop = asyncio.get_event_loop()
    app = Application(loop)
    app.run()


if __name__ == '__main__':
    main()
